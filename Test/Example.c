#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


int main(int argc, char **argv)
{
	printf("Hello from printf %s\n", "world!");

	void *data;
	data = malloc(512);

	memset(data, 0x41, 42);

	write(1, data, 42); 
	free(data);

	sleep(5);

	return 0;
}