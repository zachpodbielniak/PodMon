#ifndef PODMON_IOORIGINAL_H
#define PODMON_IOORIGINAL_H


#include "../Prereqs.h"


typedef ARCHLONG (*LPFN_REAL_WRITE)(
	_In_ 		LONG		lFd, 
	_In_ 		LPVOID		lpData, 
	_In_ 		UARCHLONG	ualSize
);


typedef ARCHLONG (*LPFN_REAL_READ)(
	_In_ 		LONG 		lFd,
	_Out_ 		LPVOID 		lpDataBuffer,
	_In_ 		UARCHLONG	ualBufferSize
);


typedef LONG (*LPFN_REAL_OPEN)(
	_In_Z_ 		LPCSTR 		lpcszFilePath,
	_In_		LONG 		lOpenFlag,
	_In_Opt_ 	...
);


typedef LONG (*LPFN_REAL_CLOSE)(
	_In_ 		LONG 		lFd
);


typedef struct __REAL_IO_PROCS
{
	LPFN_REAL_OPEN		lpfnRealOpen;
	LPFN_REAL_CLOSE 	lpfnRealClose;
	LPFN_REAL_READ 		lpfnRealRead;
	LPFN_REAL_WRITE		lpfnRealWrite;
} REAL_IO_PROCS, *LPREAL_IO_PROCS;


GLOBAL_VARIABLE DONT_OPTIMIZE_OUT REAL_IO_PROCS riopIoProcs;



#endif