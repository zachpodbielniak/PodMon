#include "IoOriginal.h"
#include "../DataPoints.h"



ARCHLONG
write(
	_In_ 		LONG 		lFd,
	_In_		LPVOID 		lpData,
	_In_ 		UARCHLONG 	ualSize
){
	ARCHLONG alRet;

	if (NULLPTR == riopIoProcs.lpfnRealWrite)
	{ riopIoProcs.lpfnRealWrite = (LPFN_REAL_WRITE)dlsym(RTLD_NEXT, "write"); }

	alRet = riopIoProcs.lpfnRealWrite(lFd, lpData, ualSize);

	if (0 < alRet)
	{
		InterlockedAdd(&(dpRunningData.ualFileWriteBytes), alRet);
		InterlockedAdd(&(dpRunningData.ualFileWriteBytesRequested), ualSize);
	}

	return alRet;
}



ARCHLONG 
read(
	_In_ 		LONG 		lFd,
	_In_		LPVOID 		lpDataBuffer,
	_In_ 		UARCHLONG 	ualBufferSize
){
	ARCHLONG alRet;

	if (NULLPTR == riopIoProcs.lpfnRealRead)
	{ riopIoProcs.lpfnRealRead = (LPFN_REAL_READ)dlsym(RTLD_NEXT, "read"); }

	alRet = riopIoProcs.lpfnRealRead(lFd, lpDataBuffer, ualBufferSize);

	if (0 < alRet)
	{
		InterlockedAdd(&(dpRunningData.ualFileReadBytes), alRet);
		InterlockedAdd(&(dpRunningData.ualFileReadBytesRequested), ualBufferSize);
	}
	
	return alRet;
}



LONG 
close(
	_In_		LONG 		lFd
){
	LONG lRet;

	if (NULLPTR == riopIoProcs.lpfnRealClose)
	{ riopIoProcs.lpfnRealClose = (LPFN_REAL_CLOSE)dlsym(RTLD_NEXT, "close"); }

	lRet = riopIoProcs.lpfnRealClose(lFd);
	return lRet;
}