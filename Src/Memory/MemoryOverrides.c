#include "MemoryOriginal.h"
#include "../DataPoints.h"



VOID 
free(
	_In_ 		LPVOID 			lpLocation
){
	if (NULLPTR == rmpMemProcs.lpfnRealFree)
	{ rmpMemProcs.lpfnRealFree = (LPFN_REAL_FREE)dlsym(RTLD_NEXT, "free"); }

	rmpMemProcs.lpfnRealFree(lpLocation);

	if (NULLPTR != lpLocation)
	{
		UARCHLONG ualSize;

		ualSize = *(LPUARCHLONG)(LPVOID)((UARCHLONG)lpLocation - 8);
		ualSize = (ualSize % 2 == 0) ? ualSize : ualSize ^ 0x01;

		InterlockedAdd(&(dpRunningData.ualMemoryFreedBytes), ualSize);
		InterlockedIncrement(&(dpRunningData.ualMemoryFreeCalls));
	}

	return;
}



LPVOID 
malloc(
	_In_ 		UARCHLONG 		ualRequestedSize
){
	LPVOID lpRet; 

	if (NULLPTR == rmpMemProcs.lpfnRealMalloc)
	{ rmpMemProcs.lpfnRealMalloc = (LPFN_REAL_MALLOC)dlsym(RTLD_NEXT, "malloc"); }

	lpRet = rmpMemProcs.lpfnRealMalloc(ualRequestedSize);

	if (NULLPTR != lpRet)
	{
		InterlockedAdd(&(dpRunningData.ualMemoryMallocationBytes), ualRequestedSize);
		InterlockedIncrement(&(dpRunningData.ualMemoryMallocCalls));
	}

	return lpRet;
}