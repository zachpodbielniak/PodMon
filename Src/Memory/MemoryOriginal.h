#ifndef PODMON_MEMORYORIGINAL_H
#define PODMON_MEMORYORIGINAL_H


#include "../Prereqs.h"




typedef LPVOID (*LPFN_REAL_MALLOC)(
	_In_ 		UARCHLONG 		ualRequestedSize
);




typedef LPVOID (*LPFN_REAL_CALLOC)(
	_In_ 		UARCHLONG 		ualNumberOfItems,
	_In_ 		UARCHLONG 		ualItemSize
);




typedef LPVOID (*LPFN_REAL_REALLOC)(
	_In_ 		LPVOID 			lpLocation,
	_In_		UARCHLONG 		ualNewSize
);




typedef VOID (*LPFN_REAL_FREE)(
	_In_ 		LPVOID 			lpLocation
);




typedef struct __REAL_MEMORY_PROCS
{
	LPFN_REAL_MALLOC		lpfnRealMalloc;
	LPFN_REAL_CALLOC		lpfnRealCalloc;
	LPFN_REAL_REALLOC		lpfnRealReAlloc;
	LPFN_REAL_FREE			lpfnRealFree;
} REAL_MEMORY_PROCS, *LPREAL_MEMORY_PROCS;




GLOBAL_VARIABLE DONT_OPTIMIZE_OUT REAL_MEMORY_PROCS rmpMemProcs;




#endif