#include "Prereqs.h"
#include "DataPoints.h"
#include "Io/IoOriginal.h"


/*
 	Available Externally to other files, declared here to make sure
	that it is available during __Constructor() and __Destructor()
	are called
*/
DONT_OPTIMIZE_OUT DATA_POINTS dpRunningData = {0};




VOID __Constructor(
	VOID
) __attribute__((constructor));




VOID __Destructor(
	VOID
) __attribute__((destructor));




VOID 
__Constructor(
	VOID
){
	ZeroMemory((LPVOID)&riopIoProcs, sizeof(REAL_IO_PROCS));
	ZeroMemory((LPVOID)&dpRunningData, sizeof(DATA_POINTS));

	PrintFormat("\n");
	PrintFormat("================================\n");
	PrintFormat("PodMon Loaded\n");
	PrintFormat("================================\n\n");

	return;
}




VOID 
__Destructor(
	VOID
){
	PrintFormat("\n");
	PrintFormat("================================\n");
	PrintFormat("PodMon Summary\n");
	PrintFormat("================================\n");
	PrintFormat("Bytes Wrote:\t%hu / %hu\n", dpRunningData.ualFileWriteBytes, dpRunningData.ualFileWriteBytesRequested);
	PrintFormat("Bytes Read:\t%hu / %hu\n", dpRunningData.ualFileReadBytes, dpRunningData.ualFileReadBytesRequested);
	PrintFormat("Memory (M / C / R / F):\t%hu / %hu / %hu / %hu\n", dpRunningData.ualMemoryMallocationBytes, dpRunningData.ualMemoryCallocationBytes, dpRunningData.ualMemoryReAllocationBytes, dpRunningData.ualMemoryFreedBytes);
	return;
}

