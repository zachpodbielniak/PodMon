#ifndef PODMOD_PREREQS_H
#define PODMON_PREREQS_H

#define _GNU_SOURCE
#define __NO_UNISTD

#include <dlfcn.h>
#include <PodNet/TypeDefs.h>
#include <PodNet/CNetworking/CNetworking.h>
#include <PodNet/CThread/CThread.h>
#include <PodNet/CString/CString.h>
#include <PodNet/CString/CHeapString.h>

#endif