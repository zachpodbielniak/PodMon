#ifndef PODMON_LOADERORIGINAL_H
#define PODMON_LOADERORIGINAL_H


#include "../Prereqs.h"


typedef LONG (*LPFN_REAL_LIBCSTARTMAIN)(
	_In_ 		LONG 		(*lpfnMain)(LONG, DLPSTR, DLPSTR),
	_In_ 		LONG		lArgCount,
	_In_Z_ 		DLPSTR		dlpszArgValues,
	_In_ 		VOID 		(*lpfnInit)(VOID),
	_In_ 		VOID 		(*lpfnFini)(VOID),
	_In_ 		VOID 		(*lpfnRtldFini)(VOID),
	_In_ 		VOID 		(*lpStackEnd)
);


#endif