#include "LoaderOriginal.h"




INTERNAL_OPERATION
LPVOID 
PodMonMain(
	_In_Opt_	LPVOID 		lpData
){
	INFINITE_LOOP()
	{
		PrintFormat("PodMonMain()\n");
		Sleep(1000);
	}

	return NULLPTR;
}




LONG 
__libc_start_main(
	_In_ 		LONG 		(*lpfnMain)(LONG, DLPSTR, DLPSTR),
	_In_ 		LONG		lArgCount,
	_In_Z_ 		DLPSTR		dlpszArgValues,
	_In_ 		VOID 		(*lpfnInit)(VOID),
	_In_ 		VOID 		(*lpfnFini)(VOID),
	_In_ 		VOID 		(*lpfnRtldFini)(VOID),
	_In_ 		VOID 		(*lpStackEnd)
){
	LPFN_REAL_LIBCSTARTMAIN lpfnRealLibcStartMain;

	lpfnRealLibcStartMain = (LPFN_REAL_LIBCSTARTMAIN)dlsym(RTLD_NEXT, "__libc_start_main");

	PrintFormat("__libc_start_main()\n");
	CreateThread(PodMonMain, NULLPTR, NULLPTR);

	return lpfnRealLibcStartMain(
		lpfnMain,
		lArgCount,
		dlpszArgValues,
		lpfnInit,
		lpfnFini,
		lpfnRtldFini,
		lpStackEnd
	);
}


