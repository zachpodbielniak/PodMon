#ifndef PODMON_DATAPOINTS_H
#define PODMON_DATAPOINTS_H



#include "Prereqs.h"


typedef struct __DATA_POINTS
{
	UARCHLONG		ualFileReadBytes;
	UARCHLONG		ualFileReadBytesRequested;
	UARCHLONG 		ualFileWriteBytes;
	UARCHLONG		ualFileWriteBytesRequested;
	UARCHLONG		ualFileOpened;
	UARCHLONG		ualFileClosed;

	UARCHLONG		ualMemoryMallocCalls;
	UARCHLONG		ualMemoryCallocCalls;
	UARCHLONG		ualMemoryReAllocCalls;
	UARCHLONG 		ualMemoryAllocations;
	UARCHLONG		ualMemoryReAllocations;
	UARCHLONG		ualMemoryMallocationBytes;
	UARCHLONG		ualMemoryCallocationBytes;
	UARCHLONG 		ualMemoryReAllocationBytes;
	UARCHLONG 		ualMemoryFreedBytes;
	UARCHLONG		ualMemoryFreeCalls;
} DATA_POINTS, *LPDATA_POINTS;


EXTERN DONT_OPTIMIZE_OUT DATA_POINTS dpRunningData;


#endif