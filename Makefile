all:
	gcc -g -shared -nostdlib -fPIC -ldl -lpodnet_d -o bin/libpodmon_d.so Src/EntryPoint.c Src/Io/IoOverrides.c Src/Memory/MemoryOverrides.c Src/Loader/LoaderOverrides.c

example:
	gcc -g -o bin/Example Test/Example.c